 const content = require('./content.ejs');
 const layout = require('layout');

 module.exports = layout.init({
   pageTitle: 'home page',
 }).run(content());
