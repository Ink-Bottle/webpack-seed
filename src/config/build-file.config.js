require('!!file-loader?name=index.html!../index.html');
module.exports = {
  js: {
    html5shiv: require('!!file-loader?name=assets/js/[name].[ext]!../../vendor/ie-fix/html5shiv.min.js'),
    respond: require('!!file-loader?name=assets/js/[name].[ext]!../../vendor/ie-fix/respond.min.js'),
    jquery: require('!!file-loader?name=assets/js/[name].[ext]!jquery/dist/jquery.min.js'),
  },
  images: {
   // 'login-bg': require('!!file-loader?name=assets/images/[name].[ext]!../../public/images/login-bg.jpg'),
  },
  dll: {
    js: require('!!file-loader?name=assets/dll/dll.js!../dll/dll.js'),
    css: require('!file-loader?name=assets/dll/dll.css!../dll/dll.css'),
  },
};
