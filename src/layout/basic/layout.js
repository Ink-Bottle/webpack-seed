const config = require('configModule');
const noJquery = require('withoutJqueryModule');
const layout = require('./tpl.ejs'); // 整个页面布局的模板文件，主要是用来统筹各个公共组件的结构
const header = require('__COMPONENT__/header');
const footer = require('__COMPONENT__/footer');

/* 整理渲染公共部分所用到的模板变量 */
const pf = {
  pageTitle: '',
  constructInsideUrl: noJquery.constructInsideUrl,
};

module.exports = {
  init({pageTitle: pageTitle = 'this is page title'}) {
    pf.pageTitle = pageTitle;
    return this;
  },
  /* 整合各公共组件和页面实际内容，最后生成完整的HTML文档 */
  run(content) {
    const componentRenderData = Object.assign({}, config, pf); // 页头组件需要加载css/js等，因此需要比较多的变量
    const renderData = {
      header: header(componentRenderData),
      footer: footer(componentRenderData),
      // topNav: '<p>this is topNav</p>',
      // sideMenu: '<p>this is sideMenu</p>',
      content,
    };

    return layout(renderData);
  },
};
