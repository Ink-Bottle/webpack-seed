module.exports = {
	entry: require('./wp-config/entry.config.js'),
	output: require('./wp-config/output.config.js'),
	module: require('./wp-config/module.dev.config.js'),
	resolve: require('./wp-config/resolve.config.js'),
	plugins: require('./wp-config/plugins.dev.config.js'),
	externals: require('./wp-config/externals.config.js')
}