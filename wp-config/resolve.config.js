const path = require('path');
const dirVars = require('./base/dir-vars.config.js');

module.exports = {
	// 模块别名的配置
	alias: {
		/* 各种目录 */
		__IMAGE__: path.resolve(dirVars.publicDir,'images/'),
        __CSS__: path.resolve(dirVars.publicDir,'css/'),
        __FONTS__: path.resolve(dirVars.publicDir, 'fonts/'),
        __LESS__: path.resolve(dirVars.publicDir, 'less/'),
        __SCSS__: path.resolve(dirVars.publicDir, 'scss/'),
        __VENDOR__: dirVars.vendorDir,
        __CONFIG__: dirVars.configDir,
        __COMPONENT__: path.resolve(dirVars.srcRootDir, 'components/'),
       

		/* vendor */
    	/* bootstrap 相关 */
    	/* libs */
        withoutJqueryModule: path.resolve(dirVars.libsDir, 'without-jquery.module'),

    	/* less */
    	/* components */
    	/* layout */
    	layout: path.resolve(dirVars.layoutDir, 'basic/layout'),

        /* logic */
        __COMMON__: path.resolve(dirVars.logicDir, 'common.module'),
        __COMMON__PAGE__: path.resolve(dirVars.logicDir, 'common.page'),

    	/* config */
    	configModule: path.resolve(dirVars.configDir, 'common.config'),
	},
    // 当require的模块找不到时，尝试添加这些后缀后进行寻找
    extensions: ['.js', '.css', '.less','.ejs'],
}