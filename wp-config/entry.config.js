const path = require('path');
const dirVars = require('./base/dir-vars.config.js');
const pageArr = require('./base/page-entries.config.js');

let configEntry = {};

pageArr.forEach((page) => {
  configEntry[page] = path.resolve(dirVars.pagesDir, page + '/page');
});

module.exports = configEntry;